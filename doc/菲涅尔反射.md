### 菲涅尔反射

菲涅耳效应，根据观察角度产生不同反射率从而对表面效果产生影响。Fresnel节点计算公式：

```glsl
float computeFresnelTerm(vec3 viewDirection, vec3 worldNormal, float bias, float power)
{
    float fresnelTerm = pow(bias + abs(dot(viewDirection, worldNormal)), power);
    return clamp(fresnelTerm, 0., 1.);
}
```

![菲涅尔反射](https://gitee.com/effect-exporter/effect-exporter-examples/raw/master/img/%E8%8F%B2%E6%B6%85%E5%B0%94%E5%8F%8D%E5%B0%84.gif)

