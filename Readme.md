### 简介

- 插件内名称 `oreo-shader-effect-exporter`，商店内名称 `Effect Exporter`

- 这是一款为与 CocosCreator 3D 深度集成的 Shader Effect 编辑器
- 支持从资源管理器右键一键编辑 `material` 、`effect` 文件，可实时保存到effect文件并生效预览
- 支持重新编辑已导出的 effect 文件
- 支持 effect 3D 特效
- 支持 3D 光照
- 支持纹理/法线/折射/反射贴图
- 支持 `shift` + 左键 框选生成自定义节点重复使用
- 已调通 100+ 节点，可满足大部分 effect 特效需求
- 已打通在线示例，可一键导入并重复编辑

### 注意

- **仅支持 CocosCreator v3.3.0 及以上版本**

- 目前还没调试 2D 特效，有引用到 `cc-local` 等会报错，所以**目前不支持 2D effect**

- PBR 特效暂未完全支持

- 场景雾效等暂未支持

- 屏幕后期处理暂未支持

  *后续版本陆续支持*

### 使用

- 导入插件 `oreo-shader-effect-exporter` 并启用
  ![image-20211108215337808](https://gitee.com/effect-exporter/effect-exporter-examples/raw/master/img/image-20211108215337808.png)

- 新建Cube节点，新建 effect 、material文件，为 material 指定 effect 后，右键选中刚才新建的 material或effect，可看到 编辑 shader effect 相关菜单，点击即可打开 `Effect Exporter` 编辑器。简单修改下默认模板，这里是调整下颜色，点击左上角菜单 导出（.effect）即可实时导出到关联的effect文件并生成预览

  操作视频：

  <video src="https://gitee.com/effect-exporter/effect-exporter-examples/raw/master/img/effect_exporter_demo1.mp4"></video>

- 部分界面效果

  ![image-20211108232110893](https://gitee.com/effect-exporter/effect-exporter-examples/raw/master/img/image-20211108232110893.png)
  
- 加载在线示例
  
  <video src="https://gitee.com/effect-exporter/effect-exporter-examples/raw/master/img/effect_exporter_demo2.mp4"></video>
  
- CocosCreator中预览效果  
  
  <video src="https://gitee.com/effect-exporter/effect-exporter-examples/raw/master/img/effect_exporter_demo3.mp4"></video>

### 进阶

- 溶解与定向消失
  ![溶解与定向消失](https://gitee.com/effect-exporter/effect-exporter-examples/raw/master/img/%E6%BA%B6%E8%A7%A3%E4%B8%8E%E5%AE%9A%E5%90%91%E6%B6%88%E5%A4%B1.gif)

- 菲涅尔反射
  ![菲涅尔反射](https://gitee.com/effect-exporter/effect-exporter-examples/raw/master/img/%E8%8F%B2%E6%B6%85%E5%B0%94%E5%8F%8D%E5%B0%84.gif)



### Effect Exporter 目前还在持续完善中

### 欢迎到 [仓库](https://gitee.com/effect-exporter/effect-exporter-examples) 提 [issues](https://gitee.com/effect-exporter/effect-exporter-examples/issues) 

