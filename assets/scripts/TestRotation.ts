
import { _decorator, Component, Node, math, Vec3 } from 'cc';
const { ccclass, property, executeInEditMode } = _decorator;

/**
 * Predefined variables
 * Name = TestRotation
 * DateTime = Sun Nov 14 2021 21:32:43 GMT+0800 (中国标准时间)
 * Author = ihc523
 * FileBasename = TestRotation.ts
 * FileBasenameNoExtension = TestRotation
 * URL = db://assets/scripts/TestRotation.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/zh/
 *
 */
 
@ccclass('TestRotation')
@executeInEditMode(true)
export class TestRotation extends Component {
    @property({
        tooltip: "是否实时旋转"
    })
    rotating = false;

    @property({
        range: [1, 10],
        slide: true,
        tooltip: "旋转方向"
    })
    direction = new Vec3(0, -1, 0);

    @property({
        range: [1, 10],
        slide: true,
        tooltip: "旋转速度"
    })
    speed = 2;
    
    start () {
    }

    update (deltaTime: number) {
        if(!this.rotating){
            return;
        }

        let node = this.node;
        
        let q_tmp = new math.Quat();
        let v_tmp = this.direction.clone();
        v_tmp.normalize();
        let out_Q = math.Quat.rotateAround(q_tmp, node.rotation, v_tmp, Math.PI * 0.002 * this.speed);
        node.setRotation(out_Q.x, out_Q.y, out_Q.z, out_Q.w); 
    }
}
