
import { _decorator, Component, Node, setDisplayStats } from 'cc';
import {  } from 'cc/env'
const { ccclass, property } = _decorator;
 
@ccclass('TestScene')
export class TestScene extends Component {
    start () {
        setDisplayStats(true);
    }
}
